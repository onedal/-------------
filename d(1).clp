(defrule start
  (declare (salience 10))
  (initial-fact)
  =>
  (printout t "Экспертная система для нахождения проблем с пк и выдающая решение:
Отвечайте последовательно системе на вопросы, 
Вариантом ответа должно быть yes или no (y или n)" crlf)
)

(deffunction ask (?question)
  (printout t crlf ?question crlf) 
  (bind ?answer (read))
    (if (lexemep ?answer) 
    then
    (bind ?answer (lowcase ?answer)))
  ?answer
)

(deffunction ask-question (?question $?allowed-values) 
  (bind ?answer (ask ?question)) 

  (while (not (member ?answer ?allowed-values)) 
    do         
    (printout t "Введите пожалуста yes or no или помоги себе сам!" crlf)
    (bind ?answer (ask ?question))  )
  ?answer 
)

(deffunction ask_yes_no (?question)
  (bind ?response (ask-question ?question yes no y n )) 
  (if (or (eq ?response yes) (eq ?response y))
  then
  TRUE 
  else
  FALSE) 
)



; (defrule BasicReasons
; (not (power ?))
; =>
; (switch (ask-question "

; Но в начале выберите вариант, что происходит при включении pc

; 1-При нажатии кнопки включения нечего не происходит, 
; 2-При нажатии включения начинает шум и что то грузится
; 3-Затрудняюсь ответить на ваш вопрос
;   " 1 2 3) 
; (case 1 then (assert(power no)))
; (case 2 then (assert(power is)))
; (case 3 then (assert(refer to the living expert)))
; (default then (assert(power no)))
; ) )

(defrule PowerBlockOnn
(not (power ?))
=>
(if (ask_yes_no "Происходит ли, чтонибудь при нажатии включения компьютера?")
then
(assert(nextbrach))
else
(assert(power no))
) )

(defrule expertic
(nextbrach)
=>
(if (ask_yes_no "Компьютер просто шумит ?")
then
(assert(nextbrach2))
else
(assert(power is))
) )


(defrule Shooom
(nextbrach2)
=>
(if (ask_yes_no "Вы снимали недавно видеокарту,жесткий диск или процессор ?")
then
(printout t "Проверьте видеокарту,жесткий диск или процессор! " crlf)
(assert(solvedd?))
else
(assert(peremichka))
) )

(defrule ResolvedVHP
(solvedd?)
=>
(if (ask_yes_no "Ваша проблема решена?")
then
(assert(suggest "Ура проблема решена! Ваша проблема была в  неправильной установке  видеокарты ,жесткого диска или процессора")
  )
else
(assert(peremichka))
)
) 

(defrule MotherBoard
(peremichka)
=>
(if (ask_yes_no "Вы знаете где находить батарейка на материнской плате?")
then
(printout t "Попробуй обнулить биос перемычкой, которая есть на плате.! " crlf)
(assert(It is mother board!))
else
(assert(refer to the living expert))
) )



(defrule PowerBlock
(power no)
=>
(if (ask_yes_no "Проверьте подсоединен кабель в блок питания?")
then
(assert(powerblockon))
else
(printout t "Нажмите на кнопку включения которая находится взади сиситеменого блока на на блок питании и подсоедените кабель" crlf)
(assert(the problem is solved?))
) )

(defrule ResolvedPowerBlock
(the problem is solved?)
=>
(if (ask_yes_no "Ваша проблема решена?")
then
(assert(suggest "Ура проблема решена! Ваша проблема была в  подсоеденении кабеля в блок питания в розетку")
  )
else
(assert(powerblockon))
)
) 

(defrule PowerBlockOn
(powerblockon)
=>
(if (ask_yes_no "Включен ли блок питания?")
then
(assert(the power supply works))
else
(assert(the problem is solveddd?))
(printout t "Нажмите на кнопку включения которая находится взади системного блока на на блок питании" crlf)
)
) 

(defrule ResolvedBlockOn
(the problem is solveddd?)
=>
(if (ask_yes_no "Ваша проблема решена?")
then
(assert(suggest "Ура проблема решена! Ваша проблема была в в включении  блок питания")
  )
else
(assert(the power supply works))
)
) 



(defrule Filter
(the power supply works)
=>
(if (ask_yes_no "Включен ли сетевой фильтр ?")
then
(assert(network filter works))
else
(assert(the problem is solved2?))
(printout t "Нажмите на кнопку включения сетевого фильтра" crlf)
)
) 

(defrule ResolvedFilter
(the problem is solved2?)
=>
(if (ask_yes_no "Ваша проблема решена?")
then
(assert(suggest "Ваша проблема была в включении  сетевого фильтра"))
else
(assert(network filter works))
)
) 

(defrule Cable
(network filter works)
=>
(printout t crlf "Возможно проблема в целостности кабеля блок питания, замените его!" crlf)
(assert(the problem is solved3?))
)

(defrule ResolvedCable
(the problem is solved3?)
=>
(if (ask_yes_no "Ваша проблема решена?")
then
(assert(suggest "Ваша проблема в кабеле который давал питание блоку питания"))
else
(assert(refer to the living expert))
)
) 



(defrule PresencePower
(power is)
=>
(switch (ask-question "Грузится ли операционная система ?
1-Да
2-Нет
3-Не видно
  " 1 2 3) 
(case 1 then (assert(loades system)))
(case 2 then (assert(does not boot the system)))
(case 3 then (assert(no see)))
(default then (assert(no see)))
) )



(defrule PerfomancePc
(loades system)
=>
(if (ask_yes_no "У вас проблема с производительностью ?")
then
(assert(perfomance))
else
(assert(refer to the living expert))
) 
)

(defrule ResolvedPerfomance
(perfomance)
=>
(assert(suggest "Чтобы решить вашу проблему установите Ccleaner и проверьте систему"))
) 






(defrule resurrect
(does not boot the system)
=>
(if (ask_yes_no "Можете ли вы востановить систему самостоятельно ?")
then
(assert(resurrect))
else
(assert(refer to the living expert))
) 
)

(defrule ResolvedResurrect
(resurrect)
=>
(assert(suggest "У вас поломалась операционная система, востановите ее!"))
) 


(defrule NoSee
(no see)
=>
(if (ask_yes_no "Присоединен ли провод дающий питание монитору ?")
then
(assert(powermonitor))
else
(assert(powermonitoroff))
(printout t "Включите в питание провод от монитора" crlf)
)
) 

(defrule ResolvedPowerMonitorOff
(powermonitoroff)
=>
(if (ask_yes_no "Ваша проблема решена?")
then
(assert(suggest "Ваша проблема была в ключении питания монитора"))
else
(assert(powermonitor))
)
) 


(defrule MonitorOff
(powermonitor)
=>
(if (ask_yes_no "Включен ли монитор ?")
then
(assert(refer to the living expert))
else
(assert(monitoroff))
(printout t "Включите на кнопку ваш монитор" crlf)
)
) 

(defrule ResolvedMonitorOff
(monitoroff)
=>
(if (ask_yes_no "Ваша проблема решена?")
then
(assert(suggest "Ваша проблема была в включении  монитора" crlf))
else
(assert(refer to the living expert))
)
) 


(defrule HumanExpert
(refer to the living expert)
=>
(assert(suggest "Для вашей проблемы система не знает решения, вам нужно
  Обратиться к инженеру ОТП ДИТ Цуканову Д.В. ООО В-Лазер"))
) 

(defrule PrintSuggest
(suggest ?x)
=>
(printout t   ?x crlf )
)
