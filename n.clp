(defrule start
  (declare (salience 10))
  (initial-fact)
  =>
  (printout t "Давайте установим причины  с 
поломкой  ходовой части вашего автомобиля " crlf)
)

(deffunction ask (?question)
  (printout t crlf ?question crlf) 
  (bind ?answer (read))
    (if (lexemep ?answer) 
    then
    (bind ?answer (lowcase ?answer)))
  ?answer
)

(deffunction ask-question (?question $?allowed-values) 
  (bind ?answer (ask ?question)) 

  (while (not (member ?answer ?allowed-values)) 
    do         
    (printout t "Введите пожалуста yes or no" crlf)
    (bind ?answer (ask ?question))  )
  ?answer 
)

(deffunction ask_yes_no (?question)
  (bind ?response (ask-question ?question yes no y n )) 
  (if (or (eq ?response yes) (eq ?response y))
  then
  TRUE 
  else
  FALSE) 
)




; (defrule BasicReasons
; (not (machine ?))
; =>
; (switch (ask-question "
; 1-При повороте или торможении машина раскачивается
; 2-Во время движения слышен звук или скрипт
; 3-Слышен громкий скрипт при торможении или повороте
; 4-Затрудняюсь ответить на ваш вопрос
;   " 1 2 3 4) 
; (case 1 then (assert(machine swings)))
; (case 2 then (assert(machine sound or script)))
; (case 3 then (assert(machine braking)))
; (case 4 then (assert(refer to the living expert)))
; (default then (assert(refer to the living expert)))
; ) )


(defrule TrinFirst
(not (machine ?))
=>
(if (ask_yes_no "При повороте или торможении машина начинает раскачиваться?")
then
(assert(machine swings))
else
(assert(machine song))
) )


(defrule TrinTwo
(machine song)
=>
(if (ask_yes_no "Во время движения слышен звук или скрип?")
then
(assert(machine sound or script))
else
(assert(skript))
) )

(defrule TrinThree
(skript)
=>
(if (ask_yes_no "Слышен громкий скрип при торможении или повороте?")
then
(assert(machine braking))
else
(assert(YouOK))
) )

(defrule TrinQuestion
(YouOK)
=>
(if (ask_yes_no "У вас точно проблема с ходовкой?")
then
(assert(refer to the living expert))
else
(assert(refer to the living expert))
) )




(defrule MacSwings
(machine swings)
=>
(if (ask_yes_no "В последнее время сталкивались со сменой колес или их подкачкой?")
then
(assert(last time))
else
(assert(suggest "Колеса вашего автомобиля повреждены или деформированы"))
) )

(defrule SwingsCar
(last time)
=>
(if (ask_yes_no "Вибрирует весь автомобиль?")
then
(assert(all car swings))
else
(assert(reason car swings))
) 
)

(defrule SwingsAbs
(all car swings)
=>
(if (ask_yes_no "Давно проверяли амортизаторы?")
then
(assert(suggest "Амортизаторы пришли в негодность или проблема с задними колесами"))
else
(assert(suggest "Задние колеса не сбалансированы"))
) )

(defrule ReasonSwings
(reason car swings)
=>
(if (ask_yes_no "Вибрирует руль?")
then
(assert(suggest "Передние колеса не сбалансированы
  либо
 установлены под неверным углом" ))
else
(assert(suggest "Колеса установлены под неверным углом"))
) 
)


; (defrule Sound
; (machine sound or script)
; =>
; (switch (ask-question "
; Стук в подвеске или в амортизаторах?

; 1-Подвеска
; 2-Амортизатор
; 3-Затрудняюсь ответить
;   " 1 2 3 ) 
; (case 1 then (assert(pendant)))
; (case 2 then (assert(shock abs)))
; (case 3 then (assert(refer to the living expert)))
; (default then (assert(refer to the living expert)))
; ) )



(defrule Sound
(machine sound or script)
=>
(if (ask_yes_no "У вас есть стук в подвеске?")
then
(assert(pendantert))
else
(assert(pruj))
) 
)

(defrule Sound2
(pruj)
=>
(if (ask_yes_no "У вас есть стук в амортизаторах?")
then
(assert(shock abs))
else
(assert(nohelp))
) 
)

(defrule Sound3
(nohelp)
=>
(if (ask_yes_no "Вы можете разобраться самостоятельно?")
then
(assert(refer to the living expert))
else
(assert(nohelp))
) 
)




(defrule PendantYou
(pendantert)
=>
(if (ask_yes_no "В последнее время сталкивались со сменой колес?")
then
(assert(suggest "Плохая балансировка колес" ))
else
(assert(suggest "Диски деформированы или изломана пружина"))
) 
)



(defrule AbsOil
(shock abs)
=>
(if (ask_yes_no "Есть ли масло потеки под машиной?")
then
(assert(suggest "Поршень или резервуар закрыты
  либо
  из-за ударов деформирована пружина"))
else
(assert(suggest "Сломаные элементы амортизатора 
  либо
  амортизаторы плохо закреплены"))
) )


(defrule MachineBraking
(machine braking)
=>
(assert(suggest "Пружина подвески поврежедена или деформирована"))
) 



(defrule HumanExpert
(refer to the living expert)
=>
(assert(suggest "Для вашей проблемы система не знает решения, вам нужно
  В наш автосервис, где наши работники помогут установить проблему"))
) 

(defrule PrintSuggest
(suggest ?x)
=>
(printout t   ?x crlf )
)
